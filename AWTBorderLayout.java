import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.BorderLayout;
import javax.swing.*;
public class AWTBorderLayout extends Frame implements WindowListener{
	String nim;
	String nama;
	String jk;
	String alamat;
	String tmp_lahir;
	String tgl_lahir;
	TextField cnim,cnama,ctmp_lahir,ctgl_lahir;
	Choice cjk1;
	TextArea calamat;
	Button a;
	public AWTBorderLayout()
	{
		super("Data Diri");
		Panel p = new Panel();
		add(p);
		p.setLayout(new GridLayout(7, 2));
		p.add( new Label("Nim"));
		p.add(cnim = new TextField());
		p.add( new Label("Nama"));
		p.add(cnama = new TextField());
		p.add( new Label("Jenis Kelamin"));
		cjk1 = new Choice();
		cjk1.add("Laki-laki");
		cjk1.add("Perempuan");
		p.add(cjk1);
		p.add( new Label("Tempat Lahir"));
		p.add(ctmp_lahir = new TextField());
		p.add( new Label("Tanggal Lahir"));
		p.add(ctgl_lahir = new TextField());
		p.add( new Label("Alamat"));
		p.add(calamat = new TextArea());
		p.add( new Label(""));
		p.add(a=new Button("Save"),BorderLayout.LINE_END);
		setSize(250,300);
		setVisible(true);
		addWindowListener(this);
	}
	public AWTBorderLayout(String nim, String nama, String jk, String alamat, String tmp_lahir, String tgl_lahir){
				Panel h = new Panel();
				add(h);
				h.setLayout(new GridLayout(7, 2));
				h.add( new Label("Nim"));
				h.add( new Label(nim));
				h.add( new Label("Nama"));
				h.add( new Label(nama));
				h.add( new Label("Jenis Kelamin"));
				h.add( new Label(jk));
				h.add( new Label("Tempat Lahir"));
				h.add( new Label(tmp_lahir));
				h.add( new Label("Tanggal Lahir"));
				h.add( new Label(tgl_lahir));
				h.add( new Label("Alamat"));
				h.add( new Label(alamat));
				setSize(250,300);
				setVisible(true);
				addWindowListener(this);
			}
	public boolean action(Event e, Object what)
	{
		if(what.equals("Save")){
			nim = cnim.getText();
			nama = cnama.getText();
			jk = cjk1.getSelectedItem().toString();
			alamat = calamat.getText();
			tmp_lahir = ctmp_lahir.getText();
			tgl_lahir = ctgl_lahir.getText();
			new AWTBorderLayout(nim,nama,jk,alamat,tmp_lahir,tgl_lahir);
		}
		return true;
	}
	public static void main(String args[])
	{
		new AWTBorderLayout();
	}
	//Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	//Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	//Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(1);
	}
	//Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	//Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	//Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
	//Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
	}
}